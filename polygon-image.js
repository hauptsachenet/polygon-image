/**
 * example usage:
 *
 * <div data-polygon-image="{{ coverImage }}" style="width: 100%; height: 600px; margin-bottom: -600px; position: relative"></div>
 */

(function ($) {
    "use strict";

    var $document = $(document);
    var SCALE_BLUR_REDUCE = 2;
    var IE_SCALE_BLUR_REDUCE = 3;
    var DEFAULT_BLUR = 160;
    var FADE_IN_TOLERANCE = 100;
    var FADE_IN_DURATION = 400;
    var DEFAULT_NUM_DIFFERENT_IMAGES = 4;
    var DEFAULT_COLOR_RANGE = ['#999999', '#666666'];
    var DEFAULT_CELL_SIZE = 80;
    var DEFAULT_IMAGE_BRIGHTNESS = -0.25;
    var DEFAULT_POLYGON_INTENSITY = 1;

    var svgSupport = (function () {
        // use modernizr if present
        if (typeof Modernizr !== 'undefined' && typeof Modernizr.svg !== 'undefined') {
            return Modernizr.svg;
        }

        var ns = {'svg': 'http://www.w3.org/2000/svg'};
        return !!document.createElementNS && !!document.createElementNS(ns.svg, 'svg').createSVGRect;
    })();

    /**
     * Tests if the canvas is tainted by an external image.
     *
     * @param {HTMLCanvasElement} canvas
     * @returns {boolean}
     */
    var isTainted = function (canvas) {
        try {
            canvas.getContext('2d').getImageData(0, 0, 1, 1);
            return false;
        } catch (err) {
            return (err.code === 18);
        }
    };

    /**
     * @param {int} width
     * @param {int} height
     * @returns {HTMLCanvasElement}
     */
    var createHiDpiCanvas = function (width, height) {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var canvasDevicePixelRation = (window.devicePixelRatio || 1);
        // sometime the canvas calculates pixel ratio automatically
        canvasDevicePixelRation = canvasDevicePixelRation / (ctx.webkitBackingStorePixelRatio || 1);

        canvas.width = width * canvasDevicePixelRation;
        canvas.height = height * canvasDevicePixelRation;
        canvas.style.width = width + 'px';
        canvas.style.height = height + 'px';

        return canvas;
    };

    /**
     * Provides a simple scaling blur which works, even if the canvas is tainted.
     * The trade of is: it looks really ugly in some situations.
     *
     * @param {HTMLCanvasElement} originalCanvas
     * @param {int} x
     * @param {int} y
     * @param {int} width
     * @param {int} height
     * @param {int} blur
     */
    var alternativeBlurCanvas = function (originalCanvas, x, y, width, height, blur) {
        var canvas = document.createElement('canvas');

        canvas.width = Math.ceil(width / blur);
        canvas.height = Math.ceil(height / blur);

        canvas.getContext('2d').drawImage(
            originalCanvas,
            x, y, width, height,
            0, 0, canvas.width, canvas.height
        );

        originalCanvas.getContext('2d').drawImage(canvas, x, y, width, height);
    };

    /**
     * @param {string} imageSrc
     * @param {int} blur
     * @returns {jQuery}
     */
    var generateIeFallbackImage = function (imageSrc, blur) {
        var scaleFactor = blur / IE_SCALE_BLUR_REDUCE;
        var filterScaleFactor = Math.ceil(scaleFactor);

        var filter = [];
        if (filterScaleFactor > 1) {
            filter.push('progid:DXImageTransform.Microsoft.Matrix(M11="' + filterScaleFactor + '", M22="' + filterScaleFactor + '", sizingMethod="auto expand")');
        }
        filter.push('progid:DXImageTransform.Microsoft.Alpha(opacity=60)');

        var approximateOverscale = 1.1; // can make the image bigger if needed
        var sizePercent = filterScaleFactor > 1 ? Math.ceil(100 / scaleFactor * approximateOverscale) + '%' : '100%';

        var $image = $('<img>', {src: imageSrc}).css({
            filter: filter.join(' '),
            width: sizePercent,
            height: sizePercent
        });

        return $('<div>').append($image).css({
            backgroundColor: 'white',
            width: '100%',
            height: '100%'
        });
    };

    /**
     * {'_width_x_height_': [generated patterns]}
     * @type {Object.<string, Array.<{image: Image, loaded: boolean}>>}
     */
    var polygonPatternCache = {};

    /**
     * @param {Object.<string, *>} conf
     * @returns {{image: Image, loaded: boolean}}
     */
    var getPolygonPattern = function (conf) {
        var cacheKey = JSON.stringify([
            conf.width, conf.height, conf.patternColorRange, conf.cellSize
        ]);
        var cacheEntries = polygonPatternCache[cacheKey];

        if (!$.isArray(cacheEntries)) {
            cacheEntries = polygonPatternCache[cacheKey] = [];
        }

        if (cacheEntries.length >= conf.numDifferentImages) {
            return cacheEntries[Math.floor(Math.random() * cacheEntries.length)];
        }

        var polygonPattern = {
            image: new Image(),
            loaded: false
        };

        polygonPattern.image.addEventListener('load', function () {
            polygonPattern.loaded = true;
        });

        var triangleGradient = conf.patternColorRange;
        var trianglify = new Trianglify({
            x_gradient: triangleGradient,
            y_gradient: triangleGradient,
            cellsize: conf.cellSize
        });

        var pattern = trianglify.generate(conf.width, conf.height);
        polygonPattern.image.src = pattern.dataUri;

        cacheEntries.push(polygonPattern);
        return polygonPattern;
    };

    /**
     * @param {*} conf
     * @returns {HTMLElement}
     */
    var generatePolygonImage = function (conf) {
        var $element = $('<div>');

        var canvas = createHiDpiCanvas(conf.width, conf.height);
        var blurWithPixelRatio = conf.blur * (canvas.width / conf.width);

        var polygonPattern = getPolygonPattern(conf);
        var $polygonImage = $(polygonPattern.image).clone();
        $polygonImage.css({backgroundColor: 'white', opacity: 0.5}).prependTo($element);

        /**
         * Draws the triangle svg.
         *
         * @param {HTMLCanvasElement} canvas
         * @param {int} tryCount
         * @param {Function} callback
         */
        var drawSvg = function (canvas, tryCount, callback) {
            if (!polygonPattern.loaded) {
                polygonPattern.image.addEventListener('load', function () {
                    drawSvg(canvas, tryCount, callback);
                }, false);
                return;
            }

            try {
                // firefox is really bad with composite methods and svg's
                // drawing the svg onto a canvas (bitmap) first and then on the real canvas resolves this
                var svgCanvas = createHiDpiCanvas(conf.width, conf.height);
                svgCanvas.getContext('2d').drawImage(polygonPattern.image, 0, 0, svgCanvas.width, svgCanvas.height);
                canvas.getContext('2d').drawImage(svgCanvas, 0, 0, canvas.width, canvas.height);

                if ($.isFunction(callback)) {
                    callback();
                }

            } catch (e) {
                if (tryCount-- < 1) {
                    var ctx = canvas.getContext('2d');
                    ctx.fillStyle = '#999999';
                    ctx.fillRect(0, 0, canvas.width, canvas.height);
                    console.error("svg could not be drawn, image was just brightened instead", e);
                } else {
                    setTimeout(function () {
                        drawSvg(canvas, tryCount, callback);
                    }, 100);
                }
            }
        };

        // trick to notice if the page was rendered while we wait for the onload event
        // if javascript was busy, this method won't have been called until onload was triggered
        var interrupted = false;
        setTimeout(function () {
            interrupted = true;
        }, FADE_IN_TOLERANCE);

        var processImage = function () {
            var ctx = canvas.getContext('2d');
            ctx.drawImage(image, 0, 0, canvas.width, canvas.height);

            if (conf.blur > 1) {
                if (!isTainted(canvas)) {
                    stackBlurCanvasRGBA(canvas, 0, 0, canvas.width, canvas.height, blurWithPixelRatio);
                } else {
                    // if the image is tainted try different blur approach (much uglier)
                    alternativeBlurCanvas(canvas, 0, 0, canvas.width, canvas.height, blurWithPixelRatio / SCALE_BLUR_REDUCE);
                    console.warn("image is tainted, alternative blur used", conf);
                }
            }

            // darken image to lighen it later with svg
            if (conf.imageBrightness !== 0) {
                ctx.globalCompositeOperation = conf.imageBrightness > 0 ? 'lighter' : 'darker';
                ctx.globalAlpha = Math.abs(conf.imageBrightness);
                ctx.fillStyle = conf.imageBrightness > 0 ? 'white' : 'black';
                ctx.fillRect(0, 0, canvas.width, canvas.height);
            }

            // draw svg onto image
            ctx.globalCompositeOperation = 'lighter';
            ctx.globalAlpha = conf.polygonIntensity;
            drawSvg(canvas, 2, function () {

                // show canvas
                var $canvas = $(canvas).css({
                    display: 'none',
                    position: 'absolute',
                    left: 0,
                    top: 0
                });

                // if the page was rendered already use effect, otherwise just show
                if (interrupted) {
                    $canvas.appendTo($element).fadeIn(FADE_IN_DURATION, function () {
                        $element.children().not($canvas).remove();
                    });
                } else {
                    $canvas.appendTo($element.empty()).show();
                }
            });

        };

        var image = new Image();
        image.onload = processImage;
        image.src = conf.imageSrc;
        image.crossOrigin = 'anonymous';

        return $element;
    };

    $document.on('ready update.polygon.image', function (evt) {
        var $target = $(evt.target);

        $target.find('[data-polygon-image]').trigger('generate.polygon.image');
    });

    $document.on('generate.polygon.image', function (evt) {
        if (evt.isDefaultPrevented()) {
            return;
        }

        var $element = $(evt.target);
        var conf = {
            width: $element.data('width') || $element.width(),
            height: $element.data('height') || $element.height(),
            blur: parseFloat($element.data('blur') !== undefined ? $element.data('blur') : DEFAULT_BLUR),
            imageSrc: $element.data('polygon-image'),
            numDifferentImages: $element.data('num-different-images') || DEFAULT_NUM_DIFFERENT_IMAGES,
            patternColorRange: $element.data('pattern-color-range') || DEFAULT_COLOR_RANGE,
            cellSize: parseFloat($element.data('cell-size') || DEFAULT_CELL_SIZE),
            imageBrightness: parseFloat($element.data('image-brightness') || DEFAULT_IMAGE_BRIGHTNESS),
            polygonIntensity: parseFloat($element.data('polygon-intensity') || DEFAULT_POLYGON_INTENSITY)
        };

        // prevent execution without changes
        var oldConf = $element.data('polygon-image-configuration');
        if (JSON.stringify(oldConf) === JSON.stringify(conf)) {
            return
        }

        $element.data('polygon-image-configuration', conf);

        if (conf.width <= 0 || conf.height <= 0) {
            return;
        }

        try {
            if ($element.css('position') === 'static') {
                //noinspection ExceptionCaughtLocallyJS
                throw new Error("Polygon image must be positioned");
            }

            // first empty the element to be sure
            $element.empty();

            // when there is no svg support use ie filter
            if (!svgSupport) {
                $element.append(generateIeFallbackImage(conf.imageSrc, conf.blur));
                $element.trigger('generated.polygon.image');
                return;
            }

            $element.append(generatePolygonImage(conf));
            $element.trigger('generated.polygon.image');

        } catch (e) {
            $element.trigger('error.polygon.image');
            console.error('Failed to generate polygon image.', e, evt.target, conf);
        }
    });

    var resizeTimeout;
    $(window).on('resize', function () {
        clearTimeout(resizeTimeout);
        setTimeout(function () {
            $('[data-polygon-image]').trigger('generate.polygon.image');
        }, 100);
    });

})(jQuery);